# Frontend

## Badges
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=bugs)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=fga-pi2-2023-2-grupo-05_frontend&metric=coverage)](https://sonarcloud.io/summary/new_code?id=fga-pi2-2023-2-grupo-05_frontend)