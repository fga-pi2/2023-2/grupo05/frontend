FROM node:hydrogen-alpine

WORKDIR /app

RUN apk update && apk add --no-cache dumb-init

COPY --chown=node:node package*.json /app/
RUN npm ci

COPY --chown=node:node . /app/

EXPOSE 3000

CMD ["dumb-init", "npm", "start"]