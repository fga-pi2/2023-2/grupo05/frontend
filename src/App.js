import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { ChakraProvider } from "@chakra-ui/react";
import { theme } from "./assets/styles/theme";
import "react-toastify/dist/ReactToastify.css";

import { AuthProvider } from "./services/context";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Cadastro from "./pages/Cadastro";
import Acompanhamento from "./pages/Acompanhamento";
import Contato from "./pages/Contato";
import QuemSomos from "./pages/QuemSomos";
import Medicamentos from "./pages/Medicamentos";
import Perfil from "./pages/Perfil";
import MenuPage from "./pages/RadialMenuPage";

function App() {
  return (
    <BrowserRouter>
      <ChakraProvider resetCSS theme={theme}>
        <AuthProvider>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/quem-somos" element={<QuemSomos />} />
            <Route path="/contato" element={<Contato />} />
            <Route path="/login" element={<Login />} />
            <Route path="/cadastro" element={<Cadastro />} />
            <Route path="/medicamentos" element={<Medicamentos />} />
            <Route path="/acompanhamento" element={<Acompanhamento />} />
            <Route path="/perfil" element={<Perfil />} />
            <Route path="/radial-menu" element={<MenuPage />} />
          </Routes>
          <ToastContainer />
        </AuthProvider>
      </ChakraProvider>
    </BrowserRouter>
  );
}

export default App;
