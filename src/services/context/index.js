import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { api } from "../../services/api";
import { toast } from "react-toastify";

const AuthContext = createContext({});

export function AuthProvider({ children }) {
  const navigate = useNavigate();
  const location = useLocation();
  const [user, setUser] = useState(() => {
    const loadedUser = localStorage.getItem("@medHelp:user");

    if (!loadedUser) return {};

    return JSON.parse(loadedUser);
  });
  const isAuthenticated = !!user?.token;

  const signIn = useCallback(
    async (payload) => {
      try {
        const response = await api.post(`/login`, payload);

        const { caregiverId, caregiverName, takerName, phoneNumber, email } =
          response.data.user;

        const { token } = response.data.token;

        localStorage.setItem("@medHelp:token", token);
        localStorage.setItem(
          "@medHelp:user",
          JSON.stringify({
            caregiverId,
            caregiverName,
            takerName,
            phoneNumber,
            email,
          })
        );

        setUser({ caregiverId, caregiverName, takerName, phoneNumber, email, token });

        const from = location.state?.from?.pathname || "/";
        navigate(from, { replace: true });
      } catch (e) {
        toast.error(e.response.data.error);
      }
    },
    [navigate, location.state?.from?.pathname]
  );

  const signOut = useCallback(() => {
    localStorage.removeItem("@medHelp:token");
    localStorage.removeItem("@medHelp:user");

    setUser(null);

    navigate("/");
  }, [navigate]);

  const value = useMemo(
    () => ({
      signIn,
      signOut,
      isAuthenticated,
      user,
    }),
    [signIn, signOut, isAuthenticated, user]
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}
