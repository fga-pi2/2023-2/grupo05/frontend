import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Box, Flex, Text, Card } from "@chakra-ui/react";
import "swiper/css";
import "swiper/css/navigation";
import ArrowLeft from "../../assets/icons/Arrow - Left 2.svg";
import ArrowRight from "../../assets/icons/Arrow - Right 2.svg";
import { Navigation } from "swiper/modules";
import PropTypes from "prop-types";

CalendarBar.propTypes = {
  onDateSelect: PropTypes.func.isRequired,
};

export default function CalendarBar({ onDateSelect }) {
  const [selectedDate, setSelectedDate] = useState(null);
  const datas = generateArray();

  const handleDateClick = (date) => {
    setSelectedDate(date);
    onDateSelect(date);
  };

  return (
    <Flex w="100vw" bg="#063" color="white" justifyContent="center" p={4}>
      <div className="arrowContainer">
        <img
          id="arrow_back"
          src={ArrowLeft}
          alt="Arrow Back"
          style={{
            width: "65px", // Increase size as needed
            height: "65px", // Increase size as needed
            cursor: "pointer",
            transition: "transform 0.2s",
          }}
          onMouseOver={(e) => (e.currentTarget.style.transform = "scale(1.1)")}
          onMouseOut={(e) => (e.currentTarget.style.transform = "scale(1)")}
        />
      </div>
      <Swiper
        slidesPerView={7}
        spaceBetween={5}
        freeMode={true}
        navigation={{
          prevEl: "#arrow_back",
          nextEl: "#arrow_forward",
        }}
        modules={[Navigation]}
      >
        {datas.map((date, index) => (
          <SwiperSlide key={index}>
            <Flex
              direction="column"
              alignItems="center"
              justifyContent="center"
              cursor="pointer"
            >
              <Card
                w="100px"
                h="95px"
                bg="blue.600"
                borderRadius="15px"
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                boxShadow="md"
                position="relative"
                color={"white"}
                onClick={() => handleDateClick(date)}
              >
                {selectedDate?.formData === date.formData && (
                  <Box
                    position="absolute"
                    top="-10px"
                    w="20px"
                    h="20px"
                    bg="white"
                    borderRadius="full"
                  />
                )}
                <Text fontSize="lg">{date.dayOfWeek}</Text>
                <Text fontSize="lg" fontWeight="bold">
                  {date.dayOfMonth}
                </Text>
                <Text fontSize="xs">{date.month}</Text>
              </Card>
            </Flex>
          </SwiperSlide>
        ))}
      </Swiper>
      <div className="arrowContainer">
        <img
          id="arrow_forward"
          src={ArrowRight}
          alt="Arrow Forward"
          style={{
            width: "65px", // Increase size as needed
            height: "65px", // Increase size as needed
            cursor: "pointer",
            transition: "transform 0.2s",
          }}
          onMouseOver={(e) => (e.currentTarget.style.transform = "scale(1.1)")}
          onMouseOut={(e) => (e.currentTarget.style.transform = "scale(1)")}
        />
      </div>
    </Flex>
  );
}

function getDateInfo(foo) {
  const days = [
    "Domingo",
    "Segunda",
    "Terça",
    "Quarta",
    "Quinta",
    "Sexta",
    "Sábado",
  ];
  const months = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro",
  ];
  return {
    dayOfWeek: days[foo.getDay()],
    dayOfMonth: foo.getDate().toString().padStart(2, "0"),
    month: months[foo.getMonth()],
    formData:
      foo.getFullYear().toString() +
      "-" +
      (foo.getMonth() + 1) +
      "-" +
      foo.getDate().toString(),
  };
}

function generateArray() {
  let startDate = new Date();
  startDate.setDate(startDate.getDate() - 3);

  const endDate = new Date();
  endDate.setDate(endDate.getDate() + 30);

  const datesArray = [];

  while (startDate <= endDate) {
    datesArray.push(getDateInfo(startDate));
    startDate.setDate(startDate.getDate() + 1);
  }
  return datesArray;
}
