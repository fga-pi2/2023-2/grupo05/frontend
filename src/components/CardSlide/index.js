import React, { useEffect, useState } from "react";
import {
  Flex,
  Box,
  Text,
  VStack,
  HStack,
  Heading,
  Icon,
  AbsoluteCenter,
  Card,
} from "@chakra-ui/react";
import { CheckCircleIcon, CloseIcon } from "@chakra-ui/icons";
import "swiper/css";
import "swiper/css/navigation";
import PropTypes from "prop-types";

CardSlide.propTypes = {
  events: PropTypes.func.isRequired,
};

export default function CardSlide({ events }) {
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    setFilteredData(events);
  }, [events]);

  const statusIconStyles = {
    boxSize: 2.5,
    position: "absolute",
    right: 2,
    top: 2,
  };

  return (
    <Flex w="100vw" paddingTop="5rem" justify="center" align="center">
      <HStack gap="5rem">
        {filteredData.length === 0 ? (
          <AbsoluteCenter>
            <Flex
              bg="white"
              boxShadow="md"
              borderRadius="md"
              p="4"
              align="center"
              justify="center"
              h="80%"
              flexDirection="column"
              border="1px solid #036"
            >
              <Text fontSize="lg" color="#036">
                Nenhum dado correspondente à data selecionada.
              </Text>
            </Flex>
          </AbsoluteCenter>
        ) : (
          filteredData.map((event, index) => (
            <Card key={index} maxHeight="500px" overflowY="auto">
              <VStack
                bg="white"
                boxShadow="lg"
                borderRadius="md"
                p="4"
                align="start"
                spacing="4"
                m="auto"
                w="fit-content"
              >
                <Heading size="md" textAlign="center" w="full" color="#036">
                  {new Date(event.day).toLocaleDateString("pt-br")}
                </Heading>
                {event.data.map((med, index) => (
                  <Box
                    key={index}
                    p="2"
                    boxShadow="lg"
                    borderRadius="md"
                    position="relative"
                    minW="290px"
                  >
                    <Text fontSize="2xl" color="blue.600" fontWeight="bold">
                      {console.log(med)}
                      {med.medicationName}
                    </Text>
                    <Text color="blue.600">
                      <b>Descrição:</b> {med.medicationDescription}
                    </Text>
                    {med.taken ? (
                      <Icon
                        as={CheckCircleIcon}
                        color="#00ff00"
                        {...statusIconStyles}
                      />
                    ) : (
                      <Icon
                        as={CloseIcon}
                        color="#ff0000"
                        {...statusIconStyles}
                      />
                    )}
                  </Box>
                ))}
              </VStack>
            </Card>
          ))
        )}
      </HStack>
    </Flex>
  );
}
