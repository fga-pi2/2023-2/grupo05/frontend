import React, { useState, useEffect } from "react";
import {
  Box,
  Flex,
  HStack,
  VStack,
  Heading,
  Text,
  Button,
  Icon,
} from "@chakra-ui/react";
import { CheckCircleIcon } from "@chakra-ui/icons"; // Certifique-se de que este ícone está importado corretamente
import { api } from "../../services/api";
import PropTypes from "prop-types";

ListagemMedicamento.propTypes = {
  refreshRequest: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default function ListagemMedicamento({ refreshRequest, onSelect }) {
  const [filteredData, setFilteredData] = useState([]);
  const [selectedMedicationId, setSelectedMedicationId] = useState(null);

  useEffect(() => {
    fetchMedications();
  }, [refreshRequest]);

  const fetchMedications = async () => {
    try {
      const { data } = await api.get("/schedule/list/1");
      setFilteredData(data.results);
    } catch (e) {
      setFilteredData([]);
      console.error(e);
    }
  };

  const handleSelect = (medication) => {
    if (medication.scheduleId !== selectedMedicationId) {
      onSelect(medication);
      setSelectedMedicationId(medication.scheduleId);
    } else {
      onSelect(null);
      setSelectedMedicationId(null);
    }
  };

  return (
    <>
      {filteredData.length === 0 ? (
        <HStack>
          <Flex
            boxShadow="md"
            borderRadius="md"
            p="4"
            h="80px"
            mr="30px"
            w="350px"
            align="center"
            justify="center"
          >
            <Text fontSize="lg" color="blue.600">
              Nenhum Medicamento foi cadastrado.
            </Text>
          </Flex>
        </HStack>
      ) : (
        <VStack maxH="inherit" minW="380px" overflowY="auto">
          {filteredData.map((medication) => (
            <Box
              key={medication.scheduleId}
              p="10px"
              borderRadius="md"
              boxShadow="lg"
              minW="350px"
              w="full"
              bg={
                selectedMedicationId === medication.scheduleId
                  ? "green.100"
                  : ""
              }
            >
              <HStack justifyContent="space-between">
                <Flex align="center">
                  <Heading size="md" fontWeight={600} color="blue.600">
                    {medication.medicationName}
                  </Heading>
                  {selectedMedicationId === medication.scheduleId && (
                    <Icon as={CheckCircleIcon} color="green.500" ml={2} />
                  )}
                </Flex>
                <Button
                  variant="ghost"
                  onClick={() => handleSelect(medication)}
                  p="3"
                  colorScheme={
                    selectedMedicationId === medication.scheduleId
                      ? "green"
                      : undefined
                  }
                >
                  SELECIONAR
                </Button>
              </HStack>
              <HStack justifyContent="space-between">
                <Text fontWeight={600} color="blue.600">
                  Próxima Dosagem
                </Text>
                <Text p={3}>
                  {new Date(medication.nextOccurrence).toString().split("G")[0].slice(0, -4)}
                </Text>
              </HStack>
              <HStack justifyContent="space-between">
                <Text fontWeight={600} color="blue.600">
                  Descrição
                </Text>
                <Text pr={3}>{medication.medicationDescription}</Text>
              </HStack>
            </Box>
          ))}
        </VStack>
      )}
    </>
  );
}
