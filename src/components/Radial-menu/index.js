import React from "react";
import { Box, Button, Center, Text } from "@chakra-ui/react";

export const RadialMenu = ({
  numOptionsPerLayer,
  radiusIncrement,
  occupiedOptions,
  selectedSlots,
  setSelectedSlots,
}) => {
  const calculatePosition = (index, numOptions, radius, layerIndex) => {
    let offset = 90;

    const angle = (360 / numOptions) * index + offset;
    const angleInRad = (angle * Math.PI) / 180;
    const x = Math.cos(angleInRad) * radius;
    const y = Math.sin(angleInRad) * radius;
    return { x, y };
  };

  // const [selectedSlots, setSelectedSlots] = useState([]);

  const handleButtonClick = (slotId) => {
    if (selectedSlots.includes(slotId)) {
      setSelectedSlots(() => selectedSlots.filter((slot) => slot !== slotId));
    } else {
      setSelectedSlots([...selectedSlots, slotId]);
    }
  };

  return (
    <div id="radial">
      <Center height="80vh">
        <Box position="relative">
          {numOptionsPerLayer.map((numOptions, layerIndex) => {
            const radius = (layerIndex + 1) * radiusIncrement;
            return Array.from({ length: numOptions }).map((_, optionIndex) => {
              const { x, y } = calculatePosition(
                optionIndex,
                numOptions,
                radius,
                layerIndex
              );
              const slotId = `${layerIndex + 1}${(optionIndex + 1)
                .toString()
                .padStart(2, "0")}`;

              const isOccupied = occupiedOptions.includes(slotId);
              const isSelected = selectedSlots.includes(slotId);

              let buttonColor = isOccupied
                ? "#ff5042"
                : isSelected
                ? "blue.500"
                : "green.500";

              return (
                <Button
                  key={slotId}
                  onClick={() => handleButtonClick(slotId)}
                  position="absolute"
                  left="50%"
                  top="50%"
                  ml={`${x}px`}
                  mt={`${-y}px`}
                  transform="translate(-50%, -50%)"
                  borderRadius="full"
                  size="sm"
                  bg={buttonColor}
                  color="white"
                  boxShadow="md"
                  _hover={{
                    bg: isOccupied
                      ? "#f51200"
                      : isSelected
                      ? "blue.600"
                      : "green.600",
                  }}
                  sx={{
                    width: "40px",
                    height: "40px",
                    transition: "all .3s ease-in-out",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text fontSize="xs">{optionIndex + 1}</Text>
                </Button>
              );
            });
          })}
        </Box>
      </Center>
    </div>
  );
};
