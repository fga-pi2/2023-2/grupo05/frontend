import React, { useState } from "react";
import { useForm } from "react-hook-form";

import {
  Flex,
  Text,
  Heading,
  Stack,
  Button,
  Switch,
  FormLabel,
  Select,
  Box,
  Textarea,
} from "@chakra-ui/react";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { Input } from "../../components/Input";
import { toast } from "react-toastify";
import { api } from "../../services/api";
import PropTypes from "prop-types";

NovoMedicamento.propTypes = {
  setRefreshRequest: PropTypes.func.isRequired,
};

export default function NovoMedicamento({ setRefreshRequest }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    // reset,
    watch,
    // getValues
  } = useForm({
    mode: "onTouched",
    resolver: yupResolver(formSchema),
  });

  const onSubmit = handleSubmit(async (values) => {
    try {
      const { scheduleInterval, intervalHours, ...rest } = values;

      const payload = {
        caregiverId: 1,
        slots: [],
        times: scheduleInterval ? null : customTimes,
        scheduleInterval,
        intervalHours: scheduleInterval ? JSON.parse(intervalHours) : null,
        ...rest,
      };

      const response = await api.post("/schedule/new", payload);
      if (response.status === 201) {
        console.log(response);
        toast.success(response.data.message);
        setRefreshRequest.toggle();
        return;
      }
      toast.error(response.data.error);
    } catch (e) {
      toast.error("Failed to schedule");
      console.log(e);
    }
  });

  const [customTimes, setCustomTimes] = useState([""]);
  const watchScheduleInterval = watch("scheduleInterval");

  const addScheduleItem = () => {
    setCustomTimes([...customTimes, ""]);
  };

  const handleInputChange = (event, index) => {
    const newitems = [...customTimes];
    newitems[index] = event.target.value;
    setCustomTimes(newitems);
  };

  return (
    <form onSubmit={onSubmit}>
      <Flex overflowY="auto" flexDirection="column" ml="30px">
        <Heading size="xl" color="blue.600" w="100">
          Novo Medicamento
        </Heading>

        <Flex display="flex" minW="350px" flexDirection="column">
          <Text py="10px">Tipo de Cadastro</Text>
          <Flex>
            <Switch id="scheduleInterval" {...register("scheduleInterval")} />
            <FormLabel px="1rem" mb="0">
              {watchScheduleInterval ? "Padrão" : "Customizado"}
            </FormLabel>
          </Flex>
        </Flex>

        <Stack spacing="4">
          <Input
            label="Nome do Medicamento"
            type="text"
            placeholder="Aspirina 500mg"
            id="medicationName"
            name="medicationName"
            errors={errors.medicationName}
            {...register("medicationName")}
          />

          <Box>
            <>
              <Text mb="0.5rem" fontSize="1rem" color="blue.600">
                Descrição
              </Text>
              <Textarea
                label="medicationDescription"
                placeholder="Escreva os efeitos do remédio  (Até 100 caracteres)"
                type="text"
                name="medicationDescription"
                id="medicationDescription"
                errors={errors.medicationDescription}
                maxH="80px"
                {...register("medicationDescription")}
              />
              <span>{errors?.text?.message}</span>
            </>
          </Box>

          <Input
            label="De"
            type="date"
            id="startDate"
            name="startDate"
            errors={errors.startDate}
            {...register("startDate")}
          />

          <Input
            label="Até"
            type="date"
            id="endDate"
            name="endDate"
            errors={errors.endDate}
            {...register("endDate")}
          />

          {!watchScheduleInterval ? (
            <>
              <Flex
                flexDirection="column"
                alignItems="center"
                gap={5}
                maxH="184px"
                p="0px"
                overflowY="auto"
              >
                {customTimes.map((item, index) => (
                  <Input
                    key={`scheduleInterval${index}`}
                    label={`Horário ${index + 1}`}
                    type="time"
                    id={`scheduleInterval${index}`}
                    name={`scheduleInterval${index}`}
                    errors={errors[`scheduleInterval${index}`]}
                    onChange={(event) => handleInputChange(event, index)}
                  />
                ))}
              </Flex>
              <Button
                variant="ghost"
                mb="15px"
                w="100%"
                onClick={addScheduleItem}
              >
                Novo horário
              </Button>
            </>
          ) : (
            <>
              <Input
                label="Começando às"
                type="time"
                id="startTime"
                name="startTime"
                errors={errors.startTime}
                {...register("startTime")}
              />
              <Select
                id="every"
                name="intervalHours"
                errors={errors.intervalHours}
                {...register("intervalHours")}
              >
                <option disabled selected hidden>
                  A cada
                </option>
                <option value={'{ "hours": 0, "minutes": 1 }'}>1 min</option>
                <option value={'{ "hours": 0, "minutes": 2 }'}>2 min</option>
                <option value={'{ "hours": 0, "minutes": 3 }'}>3 min</option>
                <option value={'{ "hours": 0, "minutes": 4 }'}>4 min</option>
                <option value={'{ "hours": 0, "minutes": 5 }'}>5 min</option>
                <option value={'{ "hours": 4, "minutes": 0 }'}>4 horas</option>
                <option value={'{ "hours": 6, "minutes": 0 }'}>6 horas</option>
                <option value={'{ "hours": 8, "minutes": 0 }'}>8 horas</option>
                <option value={'{ "hours": 12, "minutes": 0 }'}>
                  12 horas
                </option>
                <option value={'{"hours":24,"minutes":0}'}>24 horas</option>
              </Select>
            </>
          )}
        </Stack>
        <Flex justifyContent="space-around" mt="30px">
          <Button variant="r_solid" w="50%" type="submit">
            Salvar
          </Button>
          <Button ml="15px" variant="r_solid" w="50%" type="reset">
            Cancelar
          </Button>
        </Flex>
      </Flex>
    </form>
  );
}

const formSchema = Yup.object().shape({
  medicationName: Yup.string()
    .required("Um nome é necessario")
    .min(3, "O nome deve ter mais de 2 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),

  medicationDescription: Yup.string().max(
    100,
    "A descrição deve ocntar menos de 100 caracteres"
  ),

  startDate: Yup.date().required("Campo obrigatório"),

  endDate: Yup.date().required("Campo obrigatório"),
});
