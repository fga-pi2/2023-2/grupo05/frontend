import React, { useState, useEffect } from "react";
import { Link as ReactRouterLink } from "react-router-dom";
import {
  Box,
  Image,
  Flex,
  Spacer,
  Button,
  IconButton,
  Modal,
  useDisclosure,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Text,
  Wrap,
  Badge,
  Avatar,
  VStack,
  Link as ChakraLink,
} from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import logo from "../../assets/icons/logo.svg";
import chat from "../../assets/icons/Chat.svg";

import { api } from "../../services/api";

import { useAuth } from "../../services/context";

export default function Navbar() {
  const { signOut, user } = useAuth();

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [expandedNotification, setExpandedNotification] = useState(null);
  const [notifications, setNotifications] = useState([]);
  const [unreadCount, setUnreadCount] = useState(0);

  const fetchAlerts = async () => {
    try {
      const response = await api.get("/alerts/list");
      setNotifications(response.data.results);
      const unreadNotificationsCount = response.data.results.reduce(
        (count, notification) => count + (!notification.seen ? 1 : 0),
        0
      );
      setUnreadCount(unreadNotificationsCount);
    } catch (error) {
      console.error("Erro ao buscar alertas:", error);
      setUnreadCount(0);
      setNotifications([]); // Definir como array vazio em caso de erro
    }
  };

  useEffect(() => {
    fetchAlerts();
  }, []);

  const markAsRead = async (notificationId) => {
    try {
      await api.put(`/alerts/${notificationId}`, {
        seen: true,
      });
      setUnreadCount(unreadCount - 1);
    } catch (error) {
      console.error("Erro ao marcar alerta como lido:", error);
    }
  };

  const handleNotificationClick = (index) => {
    const notification = notifications[index];
    console.log(notification);
    if (!notification.seen) {
      markAsRead(notification.alertId);
    }
    setExpandedNotification(expandedNotification === index ? null : index);
  };

  return (
    <nav shadow="medium">
      <Flex bg="#white" px={20} alignItems="center" shadow="md">
        <ChakraLink as={ReactRouterLink} to="/">
          <Image src={logo} w="60px" />
        </ChakraLink>
        <Spacer />
        {!user ? (
          <Box>
            <ChakraLink as={ReactRouterLink} to="/">
              <Button variant="ghost">Home</Button>
            </ChakraLink>
            <ChakraLink as={ReactRouterLink} to="/quem-somos">
              <Button variant="ghost">Quem Somos</Button>
            </ChakraLink>
            <ChakraLink as={ReactRouterLink} to="/contato">
              <Button variant="ghost">Contato</Button>
            </ChakraLink>
          </Box>
        ) : (
          <></>
        )}

        <Spacer />
        {user ? (
          <Wrap>
            <ChakraLink as={ReactRouterLink} to="/medicamentos">
              <Button variant="ghost">Medicamentos</Button>
            </ChakraLink>
            <ChakraLink as={ReactRouterLink} to="/radial-menu">
              <Button variant="ghost">Posição do Medicamento</Button>
            </ChakraLink>
            <ChakraLink as={ReactRouterLink} to="/acompanhamento">
              <Button variant="ghost">Acompanhamento</Button>
            </ChakraLink>

            <Wrap>
              <Button
                aria-label="Notificações"
                variant="ghost"
                onClick={onOpen}
                mx={2}
                p={2}
              >
                <img alt="notificacoes" src={chat}></img>
                {unreadCount > 0 && (
                  <Badge style={{ transform: "translate(-10px, 10px)" }}>
                    {unreadCount}
                  </Badge>
                )}
              </Button>
              <ChakraLink as={ReactRouterLink} to="/perfil">
                <Avatar size="sm" mt={1} mr={2} />
              </ChakraLink>
              <IconButton
                aria-label="logout"
                icon={<CloseIcon />}
                variant="ghost"
                onClick={signOut}
              />
            </Wrap>
          </Wrap>
        ) : (
          <>
            <ChakraLink as={ReactRouterLink} to="/login">
              <Button variant="ghost">Login</Button>
            </ChakraLink>
            <ChakraLink as={ReactRouterLink} to="/cadastro">
              <Button variant="outline">Cadastro</Button>
            </ChakraLink>
          </>
        )}

        {/* Modal de Notificações */}
        {user && (
          <Modal isOpen={isOpen} onClose={onClose} size="sm">
            <ModalOverlay />
            <ModalContent bg="#036" color="white" pb={5}>
              <ModalHeader>Notificações</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <VStack align="start" spacing={2}>
                  {Array.isArray(notifications) &&
                    notifications.map((notification, index) => (
                      <Box
                        key={index}
                        bg="white"
                        p={2}
                        borderRadius="md"
                        cursor="pointer"
                        onClick={() => handleNotificationClick(index)}
                        textAlign="left"
                        w="full"
                      >
                        <Text color="#036" fontWeight="bold">
                          {notification.message}
                        </Text>
                        {expandedNotification === index && (
                          <ChakraLink
                            as={ReactRouterLink}
                            to={notification.link}
                            color="#036"
                          >
                            Detalhes
                          </ChakraLink>
                        )}
                      </Box>
                    ))}
                </VStack>
              </ModalBody>
            </ModalContent>
          </Modal>
        )}
      </Flex>
    </nav>
  );
}
