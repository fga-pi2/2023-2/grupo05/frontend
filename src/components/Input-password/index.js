import {
	FormControl,
	FormErrorMessage,
	FormLabel,
	forwardRef,
	Input as ChakraInput,
	InputGroup,
	InputRightElement,
} from '@chakra-ui/react';

import { useState } from "react"
import showIcon from '../../assets/icons/Show.svg'
import hideIcon from '../../assets/icons/Hide.svg'

export const PasswordInput = forwardRef((props, ref) => {
	const {
	label,
	errors,
	rightElement,
	...rest
	} = props;

    const [show, setShow] = useState(false)
    const handleClick = () => setShow(!show)


	return (
		<FormControl isInvalid={Boolean(errors)}>
			{label && <FormLabel color="#003366">{label}</FormLabel>}
			<InputGroup>
				<ChakraInput {...rest} ref={ref} 
                    borderColor="blue.600"
                    type={show ? 'text' : 'password'}
                />
                <InputRightElement>
                    <img
                        src={show ? hideIcon :  showIcon}
                        alt={show ? 'Hide' : 'Show'}
                        onClick={handleClick}
                        style={{ cursor: 'pointer'}}
                    />
                </InputRightElement>
			</InputGroup>
			<FormErrorMessage>{errors?.message}</FormErrorMessage>
		</FormControl>
	);
});
