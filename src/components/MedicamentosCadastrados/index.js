import React, { useState, useEffect } from "react";
import {
  Box,
  Flex,
  HStack,
  VStack,
  Heading,
  Text,
  Button,
  Icon,
} from "@chakra-ui/react";
import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import { api } from "../../services/api";
import { toast } from "react-toastify";
import PropTypes from "prop-types";

MedicamentosCadastrados.propTypes = {
  onEdit: PropTypes.func.isRequired,
  refreshRequest: PropTypes.any.isRequired,
  setRefreshRequest: PropTypes.any.isRequired,
};

export default function MedicamentosCadastrados({
  onEdit,
  refreshRequest,
  setRefreshRequest,
}) {
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    fetchMedications();
  }, [refreshRequest]);

  const fetchMedications = async () => {
    try {
      const { data } = await api.get("/schedule/list/1");
      setFilteredData(data.results);
    } catch (e) {
      setFilteredData([]);
      console.error(e);
    }
  };

  async function sendDelete(toDelete) {
    console.log(toDelete);
    try {
      const response = await api.delete("/schedule/" + toDelete.scheduleId);

      console.log(response);
      if (response.status === 200) {
        console.log(response);
        toast.success(response.data.message);
        setRefreshRequest.toggle();
        return;
      }
      toast.error(response.data.error);
    } catch (e) {
      toast.error("Failed to delete schedule");
      console.log(e);
    }
  }

  return (
    <>
      {filteredData.length === 0 ? (
        <HStack>
          <Flex
            boxShadow="md"
            borderRadius="md"
            p="4"
            h="80px"
            mr="30px"
            w="350px"
            align="center"
            justify="center"
          >
            <Text fontSize="lg" color="blue.600">
              Nenhum Medicamento foi cadastrado.
            </Text>
          </Flex>
        </HStack>
      ) : (
        <VStack maxH="inherit" minW="380px" overflowY="auto">
          {filteredData.map((foo, index) => (
            <Box
              key={index}
              p="10px"
              borderRadius="md"
              boxShadow="lg"
              minW="350px"
            >
              <HStack justifyContent="space-between">
                <Heading size="md" Text fontWeight={600} color="blue.600">
                  {foo.medicationName}
                </Heading>
                <Flex>
                  <Button variant="ghost" onClick={() => sendDelete(foo)}>
                    <Icon as={DeleteIcon} />
                  </Button>
                  <Button variant="ghost" onClick={() => onEdit(foo)}>
                    <Icon as={EditIcon} />
                  </Button>
                </Flex>
              </HStack>
              <HStack>
                <Text fontWeight={600} color="blue.600">
                  Proxima Dosagem
                </Text>
                <Text>{new Date(foo.nextOccurrence).toString().split("G")[0].slice(0, -4)}</Text>
              </HStack>
              <HStack>
                <Text fontWeight={600} color="blue.600">
                  Descrição
                </Text>
                <Text>{foo.medicationDescription}</Text>
              </HStack>
            </Box>
          ))}
        </VStack>
      )}
    </>
  );
}

