import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  forwardRef,
  Input as ChakraInput,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";

export const Input = forwardRef((props, ref) => {
  const {
    label,
    errors,
    rightElement,
    leftElement,
    rightAddon,
    leftAddon,
    ...rest
  } = props;

  return (
    <FormControl isInvalid={Boolean(errors)}>
      {label && <FormLabel color="#003366">{label}</FormLabel>}
      <InputGroup>
        {leftAddon ?? null}
        {leftElement ?? null}
        <ChakraInput {...rest} ref={ref} borderColor="blue.600" />
        {rightElement && (
          <InputRightElement>
            {
              <img
                src={rightElement}
                style={{ cursor: "pointer" }}
                alt="Pesquisar"
              />
            }
          </InputRightElement>
        )}
        {rightAddon ?? null}
      </InputGroup>
      <FormErrorMessage>{errors?.message}</FormErrorMessage>
    </FormControl>
  );
});
