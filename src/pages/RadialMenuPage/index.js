import React, { useState, useEffect } from "react";
import Navbar from "../../components/Navbar";
import ListagemMedicamento from "../../components/ListagemMedicamento";
import {
  AbsoluteCenter,
  useBoolean,
  HStack,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Link as ChakraLink,
  VStack,
} from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";
import { RadialMenu } from "../../components/Radial-menu";
import { api } from "../../services/api";

import { toast } from "react-toastify";

export default function MenuPage() {
  const [selectedMedication, setSelectedMedication] = useState(null);
  const [refreshRequest, setRefreshRequest] = useBoolean(false);
  const [selectedSlots, setSelectedSlots] = useState([]);
  const [occupiedSlots, setOccupiedSlots] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    fetchOccupiedSlots();
  }, [refreshRequest]);

  useEffect(() => {}, [occupiedSlots]);

  const fetchOccupiedSlots = async () => {
    try {
      const response = await api.get("/slot");
      setOccupiedSlots(
        response.data.results.map(
          (item) => `${item.fileira}${item.slot.toString().padStart(2, "0")}`
        )
      );
      // console.log("fetchOccupiedSlots", occupiedSlots);
    } catch (error) {
      console.error("Erro ao buscar slots ocupados:", error);
    }
  };

  const handleConfirmSlots = () => {
    if (selectedMedication && selectedSlots.length > 0) {
      onOpen();
      return;
    }
    toast.error("Selecione o medicamento e seus slots.");
  };

  const onSubmit = async () => {
    if (!selectedMedication) {
      toast.error("Nenhum medicamento selecionado.");
      return;
    }

    try {
      const { slots, ...rest } = selectedMedication;
      console.log("onSubmit selectedMedication: ", selectedMedication);


      const payload = {
        slots: [...selectedSlots.map(Number), ...selectedMedication.slots],
        ...rest,
      };

      console.log("onSubmit payload: ", payload);

      const response = await api.put(
        "/schedule/" + selectedMedication.scheduleId,
        payload
      );

      if (response.status === 200) {
        toast.success(
          `Slots for ${selectedMedication.medicationName} updated successfully`
        );
        console.log("onSubmit response: ", response);

        setRefreshRequest.toggle();
        onClose();
        return;
      }
      toast.error(response.data.error);
    } catch (e) {
      toast.error(
        `Failed to update slots for ${selectedMedication.medicationName}`
      );
      console.log(e);
    }
  };

  return (
    <>
      <Navbar w="full" />
      <AbsoluteCenter>
        <HStack gap="15rem">
          <VStack mr="5rem">
            <Button colorScheme="blue" onClick={handleConfirmSlots} w="full">
              Confirmar Slots
            </Button>
            <ListagemMedicamento
              refreshRequest={refreshRequest}
              onSelect={setSelectedMedication}
            />
          </VStack>
          <RadialMenu
            numOptionsPerLayer={[7, 14, 21, 28]}
            radiusIncrement={50}
            occupiedOptions={occupiedSlots}
            selectedSlots={selectedSlots}
            setSelectedSlots={setSelectedSlots}
          />
        </HStack>
      </AbsoluteCenter>

      <Modal isOpen={isOpen} onClose={onClose} blockScrollOnMount={false}>
        <ModalOverlay zIndex="1400" />
        <ModalContent zIndex="1400" bg={"white"}>
          <ModalHeader>Confirmação de Seleção</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {`Deseja adicionar o remédio ${
              selectedMedication?.medicationName
            } nos slots: ${selectedSlots.join(", ")}?`}
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={5} onClick={onSubmit}>
              Confirmar
            </Button>
            <ChakraLink as={ReactRouterLink} to="/">
              <Button variant="ghost">Cancelar</Button>
            </ChakraLink>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
