import React from "react";
import Navbar from "../../components/Navbar";
import { Link as ReactRouterLink } from "react-router-dom";
import {
  Text,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  AbsoluteCenter,
  Link as ChakraLink,
  Image,
} from "@chakra-ui/react";

import unb from "../../assets/icons/unb.svg";
import elipse from "../../assets/icons/Ellipse.svg";

export default function How() {
  return (
    <div id="how">
      <div
        id="how-content"
        className="container"
      >
        <Navbar w="full" />
        <AbsoluteCenter>
          <Card w="50vw" maxW="900px" shadow="2xl" minW="580px" p={5}>
            <CardHeader>
              <Heading size="xl" justify="center">
                Quem Somos
              </Heading>
            </CardHeader>

            <CardBody>
              <Text>
                Somos um grupo de estudantes da Universidade de Brasília (UNB)
                dedicados a desenvolver soluções inovadoras no campo da saúde.
                Nosso projeto de Projeto Integrador 2 (PI2) se concentra na
                criação de um Dispenser de Medicamento Eletrônico.
                <br />
                Nossa equipe é composta por estudantes das engenharias
                automotiva, eletrônica, software, aeroespacial e de energia da
                Faculdade do Gama (FGA). Estamos unidos em nosso compromisso de
                simplificar o acesso a medicamentos e melhorar a aderência aos
                tratamentos médicos por meio da tecnologia.
                <br />
                Nosso objetivo é contribuir para um sistema de saúde mais
                eficiente e acessível, utilizando tecnologia avançada e mantendo
                a segurança dos dados dos pacientes como prioridade. Estamos
                ansiosos para enfrentar os desafios do projeto e fazer uma
                diferença positiva na vida das pessoas.
              </Text>
            </CardBody>
            <CardFooter justify="center">
              <ChakraLink as={ReactRouterLink} to="https://www.unb.br/">
                <Image alt="Unb" src={unb} />
              </ChakraLink>
            </CardFooter>
          </Card>
        </AbsoluteCenter>
        <Image alt="elipse" src={elipse} mt="100px" ml="50px" />
      </div>
    </div>
  );
}
