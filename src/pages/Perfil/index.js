import React, { useState } from "react";

import { useForm } from "react-hook-form";
import {
  Flex,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  StackDivider,
  HStack,
  Button,
  Image,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  FormControl,
  FormLabel,
  useDisclosure,
} from "@chakra-ui/react";
import { PasswordInput } from "../../components/Input-password";

import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useAuth } from "../../services/context/";
import { api } from "../../services/api";
import { toast } from "react-toastify";

import Navbar from "../../components/Navbar";
import { Input } from "../../components/Input";

import selfie from "../../assets/icons/Selfie.svg";

export default function Cadastro() {
  const { user } = useAuth();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    // watch,
    // getValues
  } = useForm({
    mode: "onTouched",
    resolver: yupResolver(formSchema),
    defaultValues: user,
  });
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onSubmit = handleSubmit(async (values) => {
    try {
      const { caregiverName, takerName, phoneNumber, email, password } = values;

      if(!password) {
        return
      }

      const payload = {
        caregiverName,
        takerName,
        phoneNumber,
        email,
        password,
      };

      console.log(payload);

      const response = await api.put(
        `/user/update/${user?.caregiverId}`,
        payload
      );
      if (response.status === 200) {
        toast.success(response.data.message);
        return;
      }
      toast.error(response.error);
    } catch (e) {
      toast.error("Failed to update user");
      console.log(e);
    }
  });

  return (
    <form id="cadastro-content" width="full" onSubmit={handleSubmit(onSubmit)}>
      <Navbar w="full" />
      <HStack p="10rem" justifyContent="center" spacing="10rem">
        <Image alt="selfie" src={selfie} />

        <Card w="50%" maxW="800px" shadow="2xl" minW="600px">
          <CardHeader>
            <Heading size="xl" color="blue.600">
              Meus Dados
            </Heading>
          </CardHeader>

          <CardBody>
            <Stack divider={<StackDivider />} spacing="4">
              <Flex gap={10}>
                <Input
                  label="Nome do Responsavel"
                  type="text"
                  placeholder="Fulano da Silva"
                  minW="250px"
                  id="caregiverName"
                  name="caregiverName"
                  errors={errors.caregiverName}
                  {...register("caregiverName")}
                />
                <Input
                  label="Nome do Tomador"
                  placeholder="Silva de Fulano"
                  type="text"
                  minW="250px"
                  id="takerName"
                  name="takerName"
                  errors={errors.takerName}
                  {...register("takerName")}
                />
              </Flex>
              <Flex gap={10}>
                <Input
                  label="E-mail"
                  placeholder="email@example.com"
                  type="email"
                  minW="250px"
                  name="email"
                  id="email"
                  errors={errors.email}
                  {...register("email")}
                />
                <Input
                  label="Telefone"
                  type="text"
                  placeholder="(12)34567-8901"
                  minW="250px"
                  name="phoneNumber"
                  errors={errors.phoneNumber}
                  id="phoneNumber"
                  {...register("phoneNumber")}
                />
              </Flex>
              <Button
                variant="r_solid"
                minW="250px"
                as="a"
                href="https://api.whatsapp.com/send/?phone=%2B14155238886&text=join+sign-suppose&type=phone_number&app_absent=0"
              >
                Clique aqui para se conectar ao Whatsapp
              </Button>
            </Stack>
          </CardBody>
          <CardFooter>
            <Flex gap="3.6rem">
              <Button
                variant="r_solid"
                bg="green.600"
                minW="250px"
                type="reset"
                onClick={reset}
              >
                Cancelar
              </Button>
              <Button variant="r_solid" minW="250px" onClick={onOpen}>
                Editar
              </Button>
            </Flex>
          </CardFooter>
        </Card>
      </HStack>
      {isOpen ? (
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent bg={"white"}>
            <ModalHeader>É necessario verificar sua senha</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <PasswordInput
                label="Senha"
                placeholder="********"
                minW="250px"
                type="password"
                name="password"
                id="password"
                errors={errors.password}
                {...register("password")}
              />
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={onSubmit}>
                Editar
              </Button>
              <Button variant="ghost" onClick={onClose}>
                Cancelar
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      ) : (
        <></>
      )}
    </form>
  );
}

const formSchema = Yup.object().shape({
  caregiverName: Yup.string()
    .required("Um nome é necessario")
    .min(4, "O nome deve ter mais de 3 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),

  phoneNumber: Yup.string()
    .required("Celular é necessario")
    .matches(
      "^\\(?(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])\\)? ?(?:[2-8]|9[0-9])[0-9]{3}\\-? ?[0-9]{4}$",
      "O numero de celular deve ser valido"
    ),

  email: Yup.string()
    .required("É necessario ter um E-mail")
    .email("O email deve ser valido"),

  takerName: Yup.string()
    .required("Um nome é necessario")
    .min(4, "O nome deve ter mais de 3 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),

  password: Yup.string()
    .required("Password is required")
});
