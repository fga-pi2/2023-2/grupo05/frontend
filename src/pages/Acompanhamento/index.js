import React, { useState, useEffect } from "react";
import { Flex } from "@chakra-ui/react";
import { api } from "../../services/api";

import Navbar from "../../components/Navbar";
import CardSlide from "../../components/CardSlide";
import CalendarBar from "../../components/Calendar";

export default function Acompanhamento() {
  const [selectedDate, setSelectedDate] = useState(null);
  const [events, setEvents] = useState(null);

  const fetchEventos = async () => {
    try {
      const { data } = await api.get(
        `/event/list/1?targetDate=${selectedDate.formData}`
      );
      setEvents(data.results);
    } catch (e) {
      setEvents([]);
      console.error(e);
    }
  };

  useEffect(() => {
    fetchEventos(selectedDate);
  }, [selectedDate]);

  const handleDateSelect = (date) => {
    setSelectedDate(date);
  };

  return (
    <Flex id="home">
      <div id="home-content" className="container">
        <Navbar />
        <CalendarBar onDateSelect={handleDateSelect} />
        {selectedDate && (
          <CardSlide events={events} selectedDate={selectedDate} />
        )}
      </div>
    </Flex>
  );
}
