import React from "react";
import { Link as ReactRouterLink } from "react-router-dom";
import { Flex, Button, Text, HStack, Image, Link as ChakraLink } from "@chakra-ui/react";
import Navbar from "../../components/Navbar";

import pessoas from "../../assets/icons/Hifive.svg";
import elipse from "../../assets/icons/Ellipse.svg";

export default function Home() {
  // const { colorMode, toggleColorMode } = useColorMode()

  return (
    <div id="home">
      <div id="home-content" className="container" style={{ overflow: 'hidden', height: '100vh' }}>
        <Navbar />
        {/* <Button onClick={toggleColorMode} size='lg'>
        			Toggle {colorMode === 'light' ? 'Dark' : 'Light'}
      			</Button> */}
        {/* <FormControl>
					<FormLabel color='blue.600'>Email</FormLabel>
					<Input placeholder='Email' />
				</FormControl> */}
        <HStack
          p="10rem"
          justifyContent="center"
          spacing="10rem"
          alignItems="start"
        >
          <Flex direction="column" color="blue.600">
            <Text fontSize="4rem" fontWeight={600}>
              Dispenser de Remedios
            </Text>
            <Text fontSize="1.5rem">O remédio certo na hora certa!</Text>
            <Flex direction="column">
              <Text fontSize="2rem" fontWeight={600} color="green.600">
                GRUPO 5
              </Text>

              <ChakraLink as={ReactRouterLink} to="/quem-somos">
                <Button variant="outline" maxWidth="240px">Saiba Mais</Button>
              </ChakraLink>
            </Flex>
          </Flex>

          <Image alt="friends" src={pessoas} />
        </HStack>
        <Image alt="elipse" src={elipse} mt="-200px" ml="-100px"/>
      </div>
    </div>
  );
}
