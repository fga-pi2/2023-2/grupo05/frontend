import React from "react";
import { useForm } from "react-hook-form";

import {
  Flex,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  StackDivider,
  Button,
  Image,
  HStack,
} from "@chakra-ui/react";

import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import Navbar from "../../components/Navbar";
import { Input } from "../../components/Input";
import { PasswordInput } from "../../components/Input-password";

import friends from "../../assets/icons/Friends.svg";

import { useAuth } from "../../services/context";

export default function Login() {
  const { signIn } = useAuth();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    resolver: yupResolver(formSchema),
  });

  const onSubmit = handleSubmit(async (values) => {
    const { email, password } = values;

    const payload = {email, password}

    await signIn(payload);
  });

  return (
    <div id="login">
      <div id="login-content" width="full">
        <Navbar w="full" />
        <HStack p="10rem" justifyContent="center" spacing="10rem">
          <Image alt="friends" src={friends} />

          <Card maxH="500px" shadow="2xl" minW="420px" p={5}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <CardHeader>
                <Heading size="xl" color="blue.600">Entrar</Heading>
              </CardHeader>

              <CardBody>
                <Stack divider={<StackDivider />} spacing="4">
                  <Flex gap={10} direction="column">
                    <Input
                      label="E-mail"
                      placeholder="email@example.com"
                      type="email"
                      minW="250px"
                      name="email"
                      id="email"
                      errors={errors.email}
                      {...register("email")}
                    />
                    <PasswordInput
                      label="Senha"
                      placeholder="********"
                      minW="250px"
                      type="password"
                      name="password"
                      id="password"
                      errors={errors.password}
                      {...register("password")}
                    />
                  </Flex>
                </Stack>
              </CardBody>
              <CardFooter justify="center">
                <Flex gap={5} direction="column">
                  <Button
                    variant="r_solid"
                    bg="green.600"
                    _hover="green.500"
                    minWidth="250px"
                    type="submit"
                  >
                    Enviar
                  </Button>
                  {/* <Button
                    variant="ghost"
                    color="green.500"
                    minW="250px"
                    type="submit"
                  >
                    Recuperar senha
                  </Button> */}
                </Flex>
              </CardFooter>
            </form>
          </Card>
        </HStack>
      </div>
    </div>
  );
}

const formSchema = Yup.object().shape({
  email: Yup.string().required("É necessario ter um E-mail"),
  password: Yup.string().required("É necessario ter uma senha"),
});
