import { AbsoluteCenter, Flex, Button, Text, useBoolean } from "@chakra-ui/react";
import React, { useState, useEffect } from "react";

import Navbar from "../../components/Navbar";
import NovoMedicamento from "../../components/NovoMedicamento";
import MedicamentosCadastrados from "../../components/MedicamentosCadastrados";
import EditarMedicamento from "../../components/EditarMedicamento";
import { Input } from "../../components/Input";
import SearchIcon from "../../assets/icons/Search.svg";
import PropTypes from "prop-types";


Medicamentos.propTypes = {
  data: PropTypes.func.isRequired,
};


export default function Medicamentos({ data }) {
  const [isEditing, setIsEditing] = useState(false);
  const [editingData, setEditingData] = useState(undefined);

  const [refreshRequest, setRefreshRequest] = useBoolean(false);

  if (data !== undefined) {
    setEditingData(data);
    setIsEditing(true);
  }

  useEffect(() => {
    handleEdit(editingData);
  }, [editingData]);

  const handleEdit = (toEdit) => {
    if (toEdit !== undefined) {
      setEditingData(toEdit);
      setIsEditing(true);
    } else {
      setEditingData(undefined);
      setIsEditing(false);
    }
  };

  return (
    <div id="medicamentos">
      <div id="medicamentos-content" width="full">
        <Navbar w="full" />
        <AbsoluteCenter>
          <Flex
            justifyContent="center"
            maxH="90vh"
            boxShadow="lg"
            p="30px"
            maxW="900px"
          >
            <Flex flexDirection="column">
              <Flex flexDirection="column" p="15px">
                <Button
                  p="10px"
                  borderRadius="md"
                  boxShadow="lg"
                  minW="350px"
                  onClick={() => handleEdit(undefined)}
                  mb="15px"
                >
                  <Text size="lg" Text fontWeight={600} color="blue.600">
                    Adicionar Medicamento
                  </Text>
                </Button>
                <Input
                  rightElement={SearchIcon}
                  placeholder="Pesquisar"
                ></Input>
              </Flex>

              <MedicamentosCadastrados onEdit={handleEdit} refreshRequest={refreshRequest} setRefreshRequest={setRefreshRequest}/>
            </Flex>

            {isEditing ? (
              <EditarMedicamento data={editingData} setRefreshRequest={setRefreshRequest}/>
            ) : (
              <NovoMedicamento setRefreshRequest={setRefreshRequest}/>
            )}
          </Flex>
        </AbsoluteCenter>
      </div>
    </div>
  );
}
