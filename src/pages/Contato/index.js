import React from "react";
import { useForm } from "react-hook-form";

import {
  Flex,
  Box,
  Text,
  Textarea,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  StackDivider,
  AbsoluteCenter,
  Button,
  Image
} from "@chakra-ui/react";

import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import Navbar from "../../components/Navbar";
import { Input } from "../../components/Input";

import elipse from "../../assets/icons/Ellipse.svg";


export default function Home() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    // reset,
    // watch,
    // getValues
  } = useForm({
    mode: "onTouched",
    resolver: yupResolver(formSchema),
  });

  function onSubmit(values) {
    return new Promise((resolve) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        resolve();
      }, 1000);
    });
  }

  return (
    <div id="contato">
      <div id="contato-content" width="full">
        <Navbar w="full" />
        <AbsoluteCenter>
          <Card w="50vw" maxW="900px" shadow="2xl" minW="580px">
            <form onSubmit={handleSubmit(onSubmit)}>
              <CardHeader>
                <Heading size="xl">Contato</Heading>
              </CardHeader>

              <CardBody>
                <Stack divider={<StackDivider />} spacing="4">
                  <Flex gap={10}>
                    <Input
                      label="Nome"
                      type="text"
                      placeholder="Fulano da Silva"
                      minW="250px"
                      id="name"
                      name="name"
                      errors={errors.name}
                      {...register("name")}
                    />
                    <Input
                      label="E-mail"
                      placeholder="email@example.com"
                      type="email"
                      minW="250px"
                      name="email"
                      id="email"
                      errors={errors.email}
                      {...register("email")}
                    />
                  </Flex>
                  <Box>
                    <>
                      <Text mb="0.5rem" fontSize="1rem" color="blue.600">
                        Assunto
                      </Text>
                      <Textarea
                        label="Text"
                        placeholder="Escreva o motivo do contato. (Até 300 caracteres)"
                        type="text"
                        minW="250px"
                        name="text"
                        id="text"
                        errors={errors.text}
                        maxHeight="300px"
                        {...register("text")}
                      />
                      <span>{errors?.text?.message}</span>
                    </>
                  </Box>
                </Stack>
              </CardBody>
              <CardFooter justify="center">
                <Button variant="r_solid" minW="250px" type="submit">
                  Enviar
                </Button>
              </CardFooter>
            </form>
          </Card>
        </AbsoluteCenter>
        <Image alt="elipse" src={elipse} mt="200px" ml="100px" objectFit="cover"/>
      </div>
    </div>
  );
}

const formSchema = Yup.object().shape({
  name: Yup.string()
    .required("Um nome é necessario")
    .min(4, "O nome deve ter mais de 3 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),

  email: Yup.string()
    .required("É necessario ter um E-mail")
    .email("O email deve ser valido"),

  text: Yup.string()
    .required("É necessario informar o motivo para contato.")
    .min(10, "O motivo deve ser melhor explicado")
    .max(
      300,
      "Desculpe, pode resumir o motivo para facilitar nosso atendimento?"
    ),
});
