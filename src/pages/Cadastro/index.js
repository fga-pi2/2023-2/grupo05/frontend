import React from "react";
import { useForm } from "react-hook-form";
import {
  Flex,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  HStack,
  Button,
  Image,
} from "@chakra-ui/react";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { api } from "../../services/api";
import { toast } from "react-toastify";

import Navbar from "../../components/Navbar";
import { Input } from "../../components/Input";
import { PasswordInput } from "../../components/Input-password";

import selfie from "../../assets/icons/Selfie.svg";

export default function Cadastro() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    resolver: yupResolver(formSchema),
  });

  const onSubmit = handleSubmit(async (values) => {
    try {
      const { caregiverName, takerName, phoneNumber, email, password } = values;

      const payload = {
        caregiverName,
        takerName,
        phoneNumber,
        email,
        password,
      };

      const response = await api.post("/register/", payload);
      if (response.status === 201) {
        toast.success(response.data.message);
        return;
      }
      toast.error(response.error);
    } catch (e) {
      toast.error("Failed to register user");
      console.log(e);
    }
  });

  return (
    <div id="cadastro">
      <form
        id="cadastro-content"
        width="full"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Navbar w="full" />
        <HStack p="5rem" justifyContent="center" spacing="10rem">
          <Image alt="selfie" src={selfie} />

          <Card w="50%" maxW="800px" shadow="2xl" minW="600px">
            <CardHeader>
              <Heading size="xl">Cadastro do Responsável</Heading>
            </CardHeader>

            <CardBody>
              <Stack spacing="4">
                <Flex gap={10}>
                  <Input
                    label="Nome"
                    type="text"
                    placeholder="Fulano da Silva"
                    minW="250px"
                    id="caregiverName"
                    name="caregiverName"
                    errors={errors.caregiverName}
                    {...register("caregiverName")}
                  />
                  <Input
                    label="Celular"
                    placeholder="(XX) XXXXX-XXXX"
                    type="text"
                    minW="250px"
                    id="phoneNumber"
                    name="phoneNumber"
                    errors={errors.phoneNumber}
                    {...register("phoneNumber")}
                  />
                </Flex>
                <Flex gap={10}>
                  <Input
                    label="E-mail"
                    placeholder="email@example.com"
                    type="email"
                    minW="250px"
                    name="email"
                    id="email"
                    errors={errors.email}
                    {...register("email")}
                  />
                  <Input
                    label="Confirmar E-mail"
                    type="email"
                    placeholder="email@example.com"
                    minW="250px"
                    name="cemail"
                    errors={errors.cemail}
                    id="cemail"
                    {...register("cemail")}
                  />
                </Flex>
                <Flex gap={10}>
                  <PasswordInput
                    label="Senha"
                    placeholder="********"
                    minW="250px"
                    type="password"
                    name="password"
                    id="password"
                    errors={errors.password}
                    {...register("password")}
                  />
                  <PasswordInput
                    label="Confirmar Senha"
                    placeholder="********"
                    type="password"
                    minW="250px"
                    name="cpassword"
                    id="cpassword"
                    errors={errors.cpassword}
                    {...register("cpassword")}
                  />
                </Flex>
              </Stack>
            </CardBody>

            <CardHeader>
              <Heading size="xl">Cadastro do Tomador</Heading>
            </CardHeader>

            <CardBody>
              <Stack spacing="4">
                <Flex gap={10} w="50%">
                  <Input
                    label="Nome"
                    type="text"
                    placeholder="Fulano da Silva"
                    minW="250px"
                    id="takerName"
                    name="takerName"
                    mr="1rem"
                    errors={errors.takerName}
                    {...register("takerName")}
                  />
                </Flex>
              </Stack>
            </CardBody>
            <CardFooter justify="center">
              <Flex gap={10}>
                <Button
                  variant="r_solid"
                  minW="250px"
                  type="submit"
                  bg="green.600"
                >
                  Cadastrar
                </Button>
                <Button variant="r_solid" minW="250px" type="reset">
                  Cancelar
                </Button>
              </Flex>
            </CardFooter>
          </Card>
        </HStack>
      </form>
    </div>
  );
}

const formSchema = Yup.object().shape({
  caregiverName: Yup.string()
    .required("Um nome é necessario")
    .min(4, "O nome deve ter mais de 3 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),

  phoneNumber: Yup.string()
    .required("Celular é necessario")
    .matches(
      "^\\(?(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])\\)? ?(?:[2-8]|9[0-9])[0-9]{3}\\-? ?[0-9]{4}$",
      "O numero de celular deve ser valido"
    ),

  email: Yup.string()
    .required("É necessario ter um E-mail")
    .email("O email deve ser valido"),
  cemail: Yup.string()
    .required("Confirmar o email é necesasrio")
    .oneOf([Yup.ref("email")], "Os emails devem ser iguais"),

  password: Yup.string()
    .required("Password is required")
    .min(
      8,
      "A senha deve conter 8 caracteres: Maiusculo, Minusculo, Numero e um especial"
    )
    .matches(
      "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
      "A senha deve conter um caractere Maiusculo, Minusculo, Numero e especial (!@#$...)"
    ),
  cpassword: Yup.string()
    .required("Confirmar a senha é necesasrio")
    .oneOf([Yup.ref("password")], "As senhas devem ser iguais"),

  takerName: Yup.string()
    .required("Um nome é necessario")
    .min(4, "O nome deve ter mais de 3 caracteres")
    .matches(/[a-zA-Z]/, "O nome deve conter apenas letras"),
});
