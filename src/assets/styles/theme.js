import { extendTheme, theme as baseTheme } from "@chakra-ui/react";
import { mode } from "@chakra-ui/theme-tools";

import { Button } from "./componentsTheme/Button";

import { colors } from "./Colors";

export const theme = extendTheme({
  ...baseTheme,
  colors,
  initialColorMode: "system",
  useSystemColorMode: false,

  shadows: {
    medium: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    large: "4px 4px 15px 0px rgba(0, 0, 0, 0.25)",
  },
  styles: {
    global: (props) => ({
      body: {
        color: mode("#000000", "blue.600")(props),
        background: "white",
        backgroundAttachment: "fixed",
        minHeight: "100vh",
        fontFamily: `'Jost', 'Overpass', sans-serif`,
      },
      html: {
        // marginLeft: "calc(100vw - 100%)", // Fix scrollbar jump
        marginRight: 0,
        fontFamily: `'Jost', 'Overpass', sans-serif`,
      },
    }),
  },
  components: {
    Button,
  },
});
