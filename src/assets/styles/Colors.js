export const colors = {
    blue: {
        '500': 'rgba(00, 33, 66, .75)',
        '600': '#003366',
    },
    green: {
        '500': 'rgba(00, 66, 33, .75)',
        '600': '#006633',
    },
    white: {
        '0':'#F7F7FF',
    },
    grey: {
        '600': '#C6C6C6',
    },
    red: "#DC493A"
};
    