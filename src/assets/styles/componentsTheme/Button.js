import { defineStyleConfig } from '@chakra-ui/react'

export const Button = defineStyleConfig({
  // The styles all button have in common
  baseStyle: {
    fontWeight: 'bold',
  },
  // Two sizes: sm and md
  sizes: {
    sm: {
      fontSize: 'sm',
      px: 4, // <-- px is short for paddingLeft and paddingRight
      py: 3, // <-- py is short for paddingTop and paddingBottom
    },
    md: {
      fontSize: 'md',
      px: 6, // <-- these values are tokens from the design system
      py: 4, // <-- these values are tokens from the design system
    },
    lg: {
      fontSize: 'lg',
      px: 10,
      py: 5,
    },
  },

  variants: {
    bleached: {
      border: 'none',
      color: "blue.600",
      bg: "transparent",
    },
    outline: {
      border: '2px solid',
      borderColor: 'blue.600',
      color: "blue.600",
      borderRadius: 'full',
    },
    r_solid: { // rounded solid
      color: 'white',
      bg: 'blue.600',
      borderRadius: 'full',
      _hover: 'blue.500',
    },
    ghost:{
      color: 'blue.600',
    }
  },
  // The default size and variant values
  defaultProps: {
    size: 'md',
    variant: 'solid',
  },
})